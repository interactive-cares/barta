<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AuthenticationController;


// Route::get('/', function () {
//     return view('welcome');
// });

Route::middleware('guest')->group( function () {
    Route::get('login', [AuthenticationController::class, 'login'])->name('login');
    Route::Post('login', [AuthenticationController::class, 'loginUser'])->name('user.login');
    Route::get('register', [AuthenticationController::class, 'register'])->name('register');
    Route::post('register', [AuthenticationController::class, 'registerUser'])->name('user.registration');
});
Route::middleware('auth')->group( function () {
    Route::get('/', [HomeController::class, 'index'])->name('home.index');
    Route::get('profile', [ProfileController::class, 'index'])->name('profile.index');
    Route::get('profile/edit', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::get('logout', [AuthenticationController::class, 'logout'])->name('user.logout');
});

