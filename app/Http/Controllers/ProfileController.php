<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ProfileRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Database\QueryException;

class ProfileController extends Controller
{
    public function index() : View {
        $user = DB::table('users')->where('id', Auth::id())->first();
        
        return view('profile.index', compact('user'));
    }

    public function edit() : View {
        $user = DB::table('users')->where('id', Auth::id())->first();

        return view('profile.edit', compact('user'));
    }

    public function update(ProfileRequest $request) : RedirectResponse {
        $validatedFields = $request->validated();

        try {
            DB::table('users')->where('id', Auth::id())->update($validatedFields);

            Toastr::success('Profile Successfully Updated !', 'Success');

            return redirect()->route('profile.index');

        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }
}
