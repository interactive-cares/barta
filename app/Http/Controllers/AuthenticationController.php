<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginUserRequest;
use Exception;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Database\QueryException;
use App\Http\Requests\RegisterUserRequest;

class AuthenticationController extends Controller
{
    public function register(): View
    {
        return view('auth.register');
    }

    public function registerUser(RegisterUserRequest $request): RedirectResponse
    {        
        $validatedFields = $request->validated();
        
        $name = explode(' ', $validatedFields['name']);
        $last_name = array_pop($name);
        $first_name = implode(' ', $name);
        $validatedFields['password'] = Hash::make($validatedFields['password']);

        unset($validatedFields['name']);

        $validatedFields = array_merge([
            'first_name'    => $first_name,
            'last_name'     => $last_name,
        ], $validatedFields);

        try {
            $newUser = DB::table('users')->insertGetId($validatedFields);

            // Log in the user
            Auth::loginUsingId($newUser);

            return redirect('/')->with('message', 'User registration successfull!');
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    public function login(): View
    {
        return view('auth.login');
    }

    public function loginUser(LoginUserRequest $request): RedirectResponse
    {
        $credentials = $request->validated();

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended('/');
        }

        return redirect()->back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ])->onlyInput('email');
    }

    public function logout(Request $request): RedirectResponse
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
