<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'first_name'    => 'required|min:3|max:30',
            'last_name'     => 'sometimes|nullable|min:3|max:10',
            'username'      => 'required|min:3|max:15|unique:users,username,'. Auth::id(),
            'email'         => 'required|email|unique:users,email,' . Auth::id(),
            'bio'           => 'sometimes|nullable'
        ];
    }
}
